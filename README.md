# GHW waveform file parser

Reading of basic waveforms is supported. Some more complex types may not be implemented yet.

Try using the mostly-upstream-compatible `ghwdump` example to inspect files:

```
$ cargo run --example ghwdump -- -hHlsSTt tests/data/counter.ghw
```
