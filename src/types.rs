use std::convert::TryFrom;
use std::fmt;
use std::marker::PhantomData;
use std::rc::Rc;

use nom::{
    combinator::{map, verify},
    number::streaming::{f64, u8},
};
use nom_leb128::{leb128_i32, leb128_i64};

use crate::parse::{Endianness, IResult, Input};
use crate::range::Range;
use crate::RuntimeInterfaceKind;

pub(crate) fn fmt_name(name: Option<&Rc<str>>) -> &str {
    match name {
        None => "<anon>",
        Some(name) => name,
    }
}

/// A type whose value can be parsed from a byte slice.
pub trait ParseValue {
    /// The rust type of values belonging to the type
    type ValueType: Copy + PartialOrd + fmt::Debug + ToString;

    /// Parse a single value of this type.
    fn parse_value<'input>(
        &self,
        endianness: Endianness,
        i: Input<'input>,
    ) -> IResult<'input, Self::ValueType>;

    /// Format a given value of this type.
    fn fmt_value(&self, val: Self::ValueType) -> String {
        val.to_string()
    }
}

/// A scalar type.
///
/// Scalar types are integer and floating-point [`Number`]s, [`Enum`]s and physical types.
/// They can be subtyped using a single [`Range`].
pub trait Scalar: ParseValue + Clone + PartialEq + fmt::Debug {
    /// Return the [`RuntimeInterfaceKind`] associated with the range of a subtype.
    fn range_rti_kind() -> RuntimeInterfaceKind;
    /// Return the type's name.
    fn name(&self) -> &Rc<str>;
}

/// An enumeration type. Consists of a number of string entries representing the different
/// enumeration values.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Enum {
    pub name: Rc<str>,
    pub variants: Vec<Rc<str>>,
}

impl fmt::Display for Enum {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "type {} is ", self.name)?;

        write!(f, "(")?;
        for (i, var) in self.variants.iter().enumerate() {
            if i != 0 {
                write!(f, ", ")?;
            }
            write!(f, "{var}")?;
        }
        write!(f, ")")
    }
}

impl ParseValue for Enum {
    type ValueType = u8;

    fn parse_value<'input>(&self, _: Endianness, i: Input<'input>) -> IResult<'input, u8> {
        verify(u8, |&n| usize::from(n) < self.variants.len())(i)
    }

    fn fmt_value(&self, val: u8) -> String {
        (*self.variants[usize::from(val)]).to_owned()
    }
}

impl Scalar for Enum {
    fn range_rti_kind() -> RuntimeInterfaceKind {
        RuntimeInterfaceKind::TypeE8
    }

    fn name(&self) -> &Rc<str> {
        &self.name
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct PhysicalUnit {
    pub name: Rc<str>,
    pub value: i64,
}

/// A physical type. Consists of a base unit name, and a list of secondary unit names with values
/// in multiples of the base unit.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Physical<T> {
    pub name: Rc<str>,
    pub primary_unit: Rc<str>,
    pub secondary_units: Vec<PhysicalUnit>,
    pub _phantom: PhantomData<T>,
}

impl<T> fmt::Display for Physical<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "type {} is range <> units", self.name)?;

        writeln!(f, "  {} = 1 {0};", self.primary_unit)?;
        for unit in &self.secondary_units {
            writeln!(f, "  {} = {} {};", unit.name, unit.value, self.primary_unit)?;
        }

        write!(f, "end units")
    }
}

impl ParseValue for Physical<i32> {
    type ValueType = i32;

    fn parse_value<'input>(&self, _: Endianness, i: Input<'input>) -> IResult<'input, i32> {
        leb128_i32(i)
    }
}

impl Scalar for Physical<i32> {
    fn range_rti_kind() -> RuntimeInterfaceKind {
        RuntimeInterfaceKind::TypeP32
    }

    fn name(&self) -> &Rc<str> {
        &self.name
    }
}

impl ParseValue for Physical<i64> {
    type ValueType = i64;

    fn parse_value<'input>(&self, _: Endianness, i: Input<'input>) -> IResult<'input, i64> {
        leb128_i64(i)
    }
}

impl Scalar for Physical<i64> {
    fn range_rti_kind() -> RuntimeInterfaceKind {
        RuntimeInterfaceKind::TypeP64
    }

    fn name(&self) -> &Rc<str> {
        &self.name
    }
}

/// A numeric type.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Number<T> {
    pub name: Rc<str>,
    pub _phantom: PhantomData<T>,
}

impl<T> fmt::Display for Number<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "type {} is range <>", self.name)
    }
}

impl ParseValue for Number<i32> {
    type ValueType = i32;

    fn parse_value<'input>(&self, _: Endianness, i: Input<'input>) -> IResult<'input, i32> {
        leb128_i32(i)
    }
}

impl Scalar for Number<i32> {
    fn range_rti_kind() -> RuntimeInterfaceKind {
        RuntimeInterfaceKind::TypeI32
    }

    fn name(&self) -> &Rc<str> {
        &self.name
    }
}

impl ParseValue for Number<f64> {
    type ValueType = f64;

    fn parse_value<'input>(
        &self,
        endianness: Endianness,
        i: Input<'input>,
    ) -> IResult<'input, f64> {
        f64(endianness.into())(i)
    }

    fn fmt_value(&self, val: Self::ValueType) -> String {
        gpoint::GPoint(val).to_string()
    }
}

impl Scalar for Number<f64> {
    fn range_rti_kind() -> RuntimeInterfaceKind {
        RuntimeInterfaceKind::TypeF64
    }

    fn name(&self) -> &Rc<str> {
        &self.name
    }
}

/// Subtype of a scalar type.
#[derive(Clone, PartialEq, Debug)]
pub struct ScalarSubtype<T: Scalar> {
    pub name: Option<Rc<str>>,
    pub base: ScalarOrSubtype<T>,
    pub range: Range<T>,
}

impl<T: Scalar> ScalarSubtype<T> {
    pub fn indication(&self) -> String {
        match &self.name {
            None => format!(
                "{} range {}",
                self.base
                    .name()
                    .expect("scalar subtype base type must be named"),
                self.range
            ),
            Some(name) => String::from(&**name),
        }
    }
}

/// A [`Scalar`] or subtype thereof.
#[derive(Clone, PartialEq, Debug)]
pub enum ScalarOrSubtype<T: Scalar> {
    Root(Rc<T>),
    Subtype(Rc<ScalarSubtype<T>>),
}

impl<T: Scalar> ScalarOrSubtype<T> {
    /// Get the root scalar type, which may be nested several levels deep.
    #[must_use]
    pub fn root(&self) -> &Rc<T> {
        match &self {
            Self::Root(t) => t,
            Self::Subtype(t) => t.base.root(),
        }
    }

    #[must_use]
    pub fn name(&self) -> Option<&Rc<str>> {
        match &self {
            Self::Root(t) => Some(t.name()),
            Self::Subtype(t) => t.name.as_ref(),
        }
    }

    #[must_use]
    pub fn fmt_as_element(&self) -> String {
        match self {
            Self::Root(t) => String::from(&**t.name()),
            Self::Subtype(t) => t.indication(),
        }
    }
}

impl<T: Scalar + fmt::Display> fmt::Display for ScalarOrSubtype<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Root(t) => t.fmt(f),
            Self::Subtype(t) => write!(
                f,
                "subtype {} is {} range {}",
                fmt_name(t.name.as_ref()),
                fmt_name(t.base.name()),
                t.range
            ),
        }
    }
}

impl<T: Scalar> ParseValue for ScalarOrSubtype<T> {
    type ValueType = T::ValueType;

    fn parse_value<'input>(
        &self,
        e: Endianness,
        i: Input<'input>,
    ) -> IResult<'input, T::ValueType> {
        match self {
            Self::Root(t) => t.parse_value(e, i),
            Self::Subtype(t) => {
                verify(|i| t.base.root().parse_value(e, i), |v| t.range.contains(v))(i)
            }
        }
    }

    fn fmt_value(&self, val: T::ValueType) -> String {
        match self {
            Self::Root(t) => t.fmt_value(val),
            Self::Subtype(t) => t.base.root().fmt_value(val),
        }
    }
}

/// An array type. Consists of an element type and a set of dimension types.
#[derive(Clone, PartialEq, Debug)]
pub struct Array {
    pub name: Rc<str>,
    pub element: Type,
    pub dimensions: Vec<Discrete>,
}

impl fmt::Display for Array {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "type {} is array (", self.name)?;

        for (i, dim) in self.dimensions.iter().enumerate() {
            if i != 0 {
                write!(f, ", ")?;
            }
            write!(
                f,
                "{} range <>",
                dim.name()
                    .expect("array subtype dimension type must be named")
            )?;
        }
        write!(f, ") of {}", self.element.fmt_as_element())
    }
}

/// Bounded subtype of an array. The element and all dimensions are bounded, which allows the
/// number of contained scalars to be calculated.
#[derive(Clone, PartialEq, Debug)]
pub struct BoundedArraySubtype {
    pub name: Option<Rc<str>>,
    pub base: ArrayOrSubtype,
    // TODO: ensure that this is a bounded subtype of the base array's element type
    pub element: Type,
    // TODO: restrict to bounded subtypes of the base array's dimension types
    pub dimensions: Vec<Discrete>,
    pub number_elements: usize,
}

impl BoundedArraySubtype {
    #[must_use]
    pub fn fmt_bounds(&self) -> String {
        format!(
            " ({})",
            self.dimensions
                .iter()
                .map(|dim| match dim {
                    Discrete::Enum(ScalarOrSubtype::Subtype(t)) => t.range.to_string(),
                    Discrete::I32(ScalarOrSubtype::Subtype(t)) => t.range.to_string(),
                    Discrete::P32(ScalarOrSubtype::Subtype(t)) => t.range.to_string(),
                    Discrete::P64(ScalarOrSubtype::Subtype(t)) => t.range.to_string(),
                    Discrete::Enum(ScalarOrSubtype::Root(_))
                    | Discrete::I32(ScalarOrSubtype::Root(_))
                    | Discrete::P32(ScalarOrSubtype::Root(_))
                    | Discrete::P64(ScalarOrSubtype::Root(_)) => unreachable!(),
                })
                .collect::<Vec<_>>()
                .join(", ")
        )
    }

    fn anonymous_indication(&self) -> String {
        format!(
            "{}{}",
            self.base
                .name()
                .expect("array subtype base type must be named"),
            self.fmt_bounds()
        )
    }

    /// Return the subtype indication. For a named subtype, this is the name; for an anonymous
    /// subtype, it is of the form `base_name (dimension_constraint[, dimension_constraint]*)`.
    #[must_use]
    pub fn indication(&self) -> String {
        self.name
            .as_ref()
            .map_or_else(|| self.anonymous_indication(), |name| String::from(&**name))
    }
}

impl fmt::Display for BoundedArraySubtype {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "subtype {} is {}",
            fmt_name(self.name.as_ref()),
            self.anonymous_indication()
        )
    }
}

/// Unbounded subtype of an array, effectively an alias.
#[derive(Clone, PartialEq, Debug)]
pub struct UnboundedArraySubtype {
    pub name: Rc<str>,
    pub base: ArrayOrSubtype,
}

impl fmt::Display for UnboundedArraySubtype {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "subtype {} is {}", self.name, fmt_name(self.base.name()),)
    }
}

/// An [`Array`] or subtype thereof.
#[derive(Clone, PartialEq, Debug)]
pub enum ArrayOrSubtype {
    Root(Rc<Array>),
    BoundedSubtype(Rc<BoundedArraySubtype>),
    UnboundedSubtype(Rc<UnboundedArraySubtype>),
}

impl ArrayOrSubtype {
    /// Get the root base type, which may be nested several levels deep.
    #[must_use]
    pub fn root(&self) -> &Rc<Array> {
        match self {
            Self::Root(t) => t,
            Self::BoundedSubtype(t) => t.base.root(),
            Self::UnboundedSubtype(t) => t.base.root(),
        }
    }

    #[must_use]
    pub fn name(&self) -> Option<&Rc<str>> {
        match &self {
            Self::Root(t) => Some(&t.name),
            Self::BoundedSubtype(t) => t.name.as_ref(),
            Self::UnboundedSubtype(t) => Some(&t.name),
        }
    }

    #[must_use]
    pub fn fmt_as_element(&self) -> String {
        match self {
            Self::Root(t) => String::from(&*t.name),
            Self::BoundedSubtype(t) => t.indication(),
            Self::UnboundedSubtype(t) => String::from(&*t.name),
        }
    }
}

impl TryFrom<Type> for ArrayOrSubtype {
    type Error = String;

    fn try_from(t: Type) -> Result<Self, String> {
        if let Type::Array(t) = t {
            Ok(t)
        } else {
            Err(format!("Invalid array base type: {t}"))
        }
    }
}

impl fmt::Display for ArrayOrSubtype {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Root(t) => t.fmt(f),
            Self::BoundedSubtype(t) => t.fmt(f),
            Self::UnboundedSubtype(t) => t.fmt(f),
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct Record {
    pub name: Rc<str>,
    pub fields: Vec<(Rc<str>, Type)>,
}

impl fmt::Display for Record {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "type {} is record", self.name)?;

        for (name, typ) in &self.fields {
            writeln!(f, "  {}: {};", name, typ.fmt_as_element())?;
        }

        write!(f, "end record")
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct BoundedRecordSubtype {
    pub name: Option<Rc<str>>,
    pub base: RecordOrSubtype,
    // TODO: ensure that these are bounded subtypes of the base records's field types
    pub fields: Vec<Type>,
}

impl BoundedRecordSubtype {
    #[must_use]
    pub fn fmt_bounds(&self) -> String {
        format!(
            "({})",
            self.fields
                .iter()
                .zip(self.base.field_types())
                .zip(self.base.field_names())
                .filter_map(|((my_type, base_type), name)| {
                    if my_type == base_type {
                        return None;
                    }

                    Some(format!(
                        "{}{}",
                        name,
                        match my_type {
                            Type::Record(RecordOrSubtype::BoundedSubtype(t)) => t.fmt_bounds(),
                            Type::Array(ArrayOrSubtype::BoundedSubtype(t)) => t.fmt_bounds(),
                            // FIXME
                            _ => unreachable!(),
                        }
                    ))
                })
                .collect::<Vec<_>>()
                .join(", ")
        )
    }

    fn anonymous_indication(&self) -> String {
        format!(
            "{}{}",
            self.base
                .name()
                .expect("record subtype base type must be named"),
            self.fmt_bounds()
        )
    }

    /// Return the subtype indication. For a named subtype, this is the name; for an anonymous
    /// subtype, it is of the form `base_name (field_constraint[, field_constraint]*)`.
    #[must_use]
    pub fn indication(&self) -> String {
        self.name
            .as_ref()
            .map_or_else(|| self.anonymous_indication(), |name| String::from(&**name))
    }
}

impl fmt::Display for BoundedRecordSubtype {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "subtype {} is {}",
            fmt_name(self.name.as_ref()),
            self.anonymous_indication()
        )
    }
}

/// Unbounded subtype of a record, effectively an alias.
#[derive(Clone, PartialEq, Debug)]
pub struct UnboundedRecordSubtype {
    pub name: Rc<str>,
    pub base: RecordOrSubtype,
}

impl fmt::Display for UnboundedRecordSubtype {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "subtype {} is {}", self.name, fmt_name(self.base.name()))
    }
}

/// A [`Record`] or subtype thereof.
#[derive(Clone, PartialEq, Debug)]
pub enum RecordOrSubtype {
    Root(Rc<Record>),
    BoundedSubtype(Rc<BoundedRecordSubtype>),
    UnboundedSubtype(Rc<UnboundedRecordSubtype>),
}

impl RecordOrSubtype {
    #[must_use]
    pub fn root(&self) -> &Rc<Record> {
        match self {
            Self::Root(t) => t,
            Self::BoundedSubtype(t) => t.base.root(),
            Self::UnboundedSubtype(t) => t.base.root(),
        }
    }

    #[must_use]
    pub fn number_scalars(&self) -> Option<usize> {
        Some(
            self.field_types()
                .map(Type::number_scalars)
                .collect::<Option<Vec<_>>>()?
                .iter()
                .product(),
        )
    }

    pub fn field_names(&self) -> impl Iterator<Item = &Rc<str>> {
        self.root().fields.iter().map(|(name, _typ)| name)
    }

    #[must_use]
    pub fn field_types(&self) -> Box<dyn Iterator<Item = &Type> + '_> {
        match self {
            Self::Root(t) => Box::new(t.fields.iter().map(|(_name, typ)| typ)),
            Self::BoundedSubtype(t) => Box::new(t.fields.iter()),
            Self::UnboundedSubtype(t) => t.base.field_types(),
        }
    }

    #[must_use]
    pub fn name(&self) -> Option<&Rc<str>> {
        match self {
            Self::Root(t) => Some(&t.name),
            Self::BoundedSubtype(t) => t.name.as_ref(),
            Self::UnboundedSubtype(t) => Some(&t.name),
        }
    }

    #[must_use]
    pub fn fmt_as_element(&self) -> String {
        match self {
            Self::Root(t) => String::from(&*t.name),
            Self::BoundedSubtype(t) => t.indication(),
            Self::UnboundedSubtype(t) => String::from(&*t.name),
        }
    }
}

impl TryFrom<Type> for RecordOrSubtype {
    type Error = String;

    fn try_from(t: Type) -> Result<Self, String> {
        if let Type::Record(t) = t {
            Ok(t)
        } else {
            Err(format!("Invalid record base type: {t}"))
        }
    }
}

impl fmt::Display for RecordOrSubtype {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Root(t) => t.fmt(f),
            Self::BoundedSubtype(t) => t.fmt(f),
            Self::UnboundedSubtype(t) => t.fmt(f),
        }
    }
}

/// An enum containing all available types behind [`Rc`] references, for easy value-level generics.
#[derive(Clone, PartialEq, Debug)]
pub enum Type {
    Enum(ScalarOrSubtype<Enum>),
    I32(ScalarOrSubtype<Number<i32>>),
    F64(ScalarOrSubtype<Number<f64>>),
    P32(ScalarOrSubtype<Physical<i32>>),
    P64(ScalarOrSubtype<Physical<i64>>),
    Array(ArrayOrSubtype),
    Record(RecordOrSubtype),
}

impl Type {
    /// Return the number of scalars contained by the type, or [`None`] if type is unbounded.
    #[must_use]
    pub fn number_scalars(&self) -> Option<usize> {
        match self {
            Self::Enum(_) | Self::I32(_) | Self::F64(_) | Self::P32(_) | Self::P64(_) => Some(1),
            Self::Array(ArrayOrSubtype::Root(_) | ArrayOrSubtype::UnboundedSubtype(_)) => None,
            Self::Array(ArrayOrSubtype::BoundedSubtype(t)) => {
                Some(t.number_elements * t.element.number_scalars()?)
            }
            Self::Record(t) => t.number_scalars(),
        }
    }

    /// Return the type's name, if any.
    #[must_use]
    pub fn name(&self) -> Option<&Rc<str>> {
        match self {
            Self::Enum(t) => t.name(),
            Self::I32(t) => t.name(),
            Self::F64(t) => t.name(),
            Self::P32(t) => t.name(),
            Self::P64(t) => t.name(),
            Self::Array(t) => t.name(),
            Self::Record(t) => t.name(),
        }
    }

    #[must_use]
    pub fn fmt_as_element(&self) -> String {
        match self {
            Self::Enum(t) => t.fmt_as_element(),
            Self::I32(t) => t.fmt_as_element(),
            Self::F64(t) => t.fmt_as_element(),
            Self::P32(t) => t.fmt_as_element(),
            Self::P64(t) => t.fmt_as_element(),
            Self::Array(t) => t.fmt_as_element(),
            Self::Record(t) => t.fmt_as_element(),
        }
    }

    #[must_use]
    pub fn root(&self) -> Type {
        match self {
            Self::Enum(t) => Self::Enum(ScalarOrSubtype::Root(Rc::clone(t.root()))),
            Self::I32(t) => Self::I32(ScalarOrSubtype::Root(Rc::clone(t.root()))),
            Self::F64(t) => Self::F64(ScalarOrSubtype::Root(Rc::clone(t.root()))),
            Self::P32(t) => Self::P32(ScalarOrSubtype::Root(Rc::clone(t.root()))),
            Self::P64(t) => Self::P64(ScalarOrSubtype::Root(Rc::clone(t.root()))),
            Self::Array(t) => Self::Array(ArrayOrSubtype::Root(Rc::clone(t.root()))),
            Self::Record(t) => Self::Record(RecordOrSubtype::Root(Rc::clone(t.root()))),
        }
    }
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Enum(t) => t.fmt(f),
            Self::I32(t) => t.fmt(f),
            Self::F64(t) => t.fmt(f),
            Self::P32(t) => t.fmt(f),
            Self::P64(t) => t.fmt(f),
            Self::Array(t) => t.fmt(f),
            Self::Record(t) => t.fmt(f),
        }
    }
}

/// A root (non-subtype) scalar type. Used for the type of signal parts.
#[derive(Clone, PartialEq, Debug)]
pub enum RootScalar {
    Enum(Rc<Enum>),
    I32(Rc<Number<i32>>),
    F64(Rc<Number<f64>>),
    P32(Rc<Physical<i32>>),
    P64(Rc<Physical<i64>>),
}

impl ParseValue for RootScalar {
    type ValueType = ScalarValue;

    fn parse_value<'input>(&self, e: Endianness, i: Input<'input>) -> IResult<'input, ScalarValue> {
        match self {
            Self::Enum(t) => map(|i| t.parse_value(e, i), ScalarValue::Enum)(i),
            Self::I32(t) => map(|i| t.parse_value(e, i), ScalarValue::I32)(i),
            Self::F64(t) => map(|i| t.parse_value(e, i), ScalarValue::F64)(i),
            Self::P32(t) => map(|i| t.parse_value(e, i), ScalarValue::I32)(i),
            Self::P64(t) => map(|i| t.parse_value(e, i), ScalarValue::I64)(i),
        }
    }

    fn fmt_value(&self, val: ScalarValue) -> String {
        match (self, val) {
            (Self::Enum(t), ScalarValue::Enum(v)) => t.fmt_value(v),
            (Self::I32(t), ScalarValue::I32(v)) => t.fmt_value(v),
            (Self::F64(t), ScalarValue::F64(v)) => t.fmt_value(v),
            (Self::P32(t), ScalarValue::I32(v)) => t.fmt_value(v),
            (Self::P64(t), ScalarValue::I64(v)) => t.fmt_value(v),
            _ => panic!("Wrong scalar value ({val:?}) for scalar type ({self:?})"),
        }
    }
}

impl TryFrom<Type> for RootScalar {
    type Error = String;

    fn try_from(t: Type) -> Result<Self, String> {
        match t {
            Type::Enum(ScalarOrSubtype::Root(t)) => Ok(Self::Enum(t)),
            Type::I32(ScalarOrSubtype::Root(t)) => Ok(Self::I32(t)),
            Type::F64(ScalarOrSubtype::Root(t)) => Ok(Self::F64(t)),
            Type::P32(ScalarOrSubtype::Root(t)) => Ok(Self::P32(t)),
            Type::P64(ScalarOrSubtype::Root(t)) => Ok(Self::P64(t)),
            Type::Enum(ScalarOrSubtype::Subtype(_))
            | Type::I32(ScalarOrSubtype::Subtype(_))
            | Type::F64(ScalarOrSubtype::Subtype(_))
            | Type::P32(ScalarOrSubtype::Subtype(_))
            | Type::P64(ScalarOrSubtype::Subtype(_))
            | Type::Array(_)
            | Type::Record(_) => Err(format!("Invalid root scalar type: {t}")),
        }
    }
}

impl fmt::Display for RootScalar {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Enum(t) => t.fmt(f),
            Self::I32(t) => t.fmt(f),
            Self::F64(t) => t.fmt(f),
            Self::P32(t) => t.fmt(f),
            Self::P64(t) => t.fmt(f),
        }
    }
}

#[derive(Clone, Copy, PartialOrd, PartialEq, Debug)]
pub enum ScalarValue {
    Enum(u8),
    I32(i32),
    I64(i64),
    F64(f64),
}

impl fmt::Display for ScalarValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Enum(x) => write!(f, "{x}"),
            Self::I32(x) => write!(f, "{x}"),
            Self::I64(x) => write!(f, "{x}"),
            Self::F64(x) => write!(f, "{x}"),
        }
    }
}

/// A discrete type. Used for the type of array dimensions.
#[derive(Clone, PartialEq, Debug)]
pub enum Discrete {
    Enum(ScalarOrSubtype<Enum>),
    I32(ScalarOrSubtype<Number<i32>>),
    P32(ScalarOrSubtype<Physical<i32>>),
    P64(ScalarOrSubtype<Physical<i64>>),
}

impl Discrete {
    fn name(&self) -> Option<&Rc<str>> {
        match self {
            Self::Enum(t) => t.name(),
            Self::I32(t) => t.name(),
            Self::P32(t) => t.name(),
            Self::P64(t) => t.name(),
        }
    }
}

impl ParseValue for Discrete {
    type ValueType = DiscreteValue;

    fn parse_value<'input>(
        &self,
        e: Endianness,
        i: Input<'input>,
    ) -> IResult<'input, DiscreteValue> {
        match self {
            Self::Enum(t) => map(|i| t.parse_value(e, i), DiscreteValue::Enum)(i),
            Self::I32(t) => map(|i| t.parse_value(e, i), DiscreteValue::I32)(i),
            Self::P32(t) => map(|i| t.parse_value(e, i), DiscreteValue::I32)(i),
            Self::P64(t) => map(|i| t.parse_value(e, i), DiscreteValue::I64)(i),
        }
    }

    fn fmt_value(&self, val: DiscreteValue) -> String {
        match (self, val) {
            (Self::Enum(t), DiscreteValue::Enum(v)) => t.fmt_value(v),
            (Self::I32(t), DiscreteValue::I32(v)) => t.fmt_value(v),
            (Self::P32(t), DiscreteValue::I32(v)) => t.fmt_value(v),
            (Self::P64(t), DiscreteValue::I64(v)) => t.fmt_value(v),
            _ => panic!("Wrong value ({val:?}) for discrete type {self:?}"),
        }
    }
}

impl TryFrom<Type> for Discrete {
    type Error = String;

    fn try_from(t: Type) -> Result<Self, String> {
        match t {
            Type::Enum(t) => Ok(Self::Enum(t)),
            Type::I32(t) => Ok(Self::I32(t)),
            Type::P32(t) => Ok(Self::P32(t)),
            Type::P64(t) => Ok(Self::P64(t)),
            Type::F64(_) | Type::Array(_) | Type::Record(_) => {
                Err(format!("Invalid discrete type: {t}"))
            }
        }
    }
}

impl From<Discrete> for Type {
    fn from(t: Discrete) -> Self {
        match t {
            Discrete::Enum(t) => Self::Enum(t),
            Discrete::I32(t) => Self::I32(t),
            Discrete::P32(t) => Self::P32(t),
            Discrete::P64(t) => Self::P64(t),
        }
    }
}

impl fmt::Display for Discrete {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Enum(t) => t.fmt(f),
            Self::I32(t) => t.fmt(f),
            Self::P32(t) => t.fmt(f),
            Self::P64(t) => t.fmt(f),
        }
    }
}

#[derive(Clone, Copy, PartialOrd, PartialEq, Debug)]
pub enum DiscreteValue {
    Enum(u8),
    I32(i32),
    I64(i64),
}

impl fmt::Display for DiscreteValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Enum(x) => write!(f, "{x}"),
            Self::I32(x) => write!(f, "{x}"),
            Self::I64(x) => write!(f, "{x}"),
        }
    }
}
