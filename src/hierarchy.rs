//! Types for representing a design hierarchy consisting of blocks and signals/ports.

use std::convert::TryInto;
use std::rc::Rc;

use nom::{combinator::map_res, number::streaming::u8};
use num_traits::FromPrimitive;

use super::{IResult, Input};
use crate::types::{RootScalar, ScalarValue, Type};

/// Type of a [`Block`].
#[derive(Clone, PartialEq, Debug)]
pub enum BlockKind {
    Design,
    Block,
    GenerateIf,
    GenerateFor {
        iterator_type: RootScalar,
        iterator_value: ScalarValue,
    },
    Instance,
    Package,
    Process,
    Generic,
}

/// Type of a [`Signal`].
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum SignalKind {
    Signal,
    PortIn,
    PortOut,
    PortInout,
    PortBuffer,
    PortLinkage,
}

/// Markers to signify the end of a [`Block`] or the hierarchy section.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum SpecialKind {
    /// End of hierarchy section
    Eoh,
    /// End of block
    Eos,
}

/// Type of a hierarchy node.
#[derive(Clone, PartialEq, Debug)]
pub enum Kind {
    Block(BlockKind),
    Signal(SignalKind),
    Special(SpecialKind),
}

impl FromPrimitive for Kind {
    fn from_i64(n: i64) -> Option<Self> {
        match n {
            0 => Some(Self::Special(SpecialKind::Eoh)),
            1 => Some(Self::Block(BlockKind::Design)),
            3 => Some(Self::Block(BlockKind::Block)),
            4 => Some(Self::Block(BlockKind::GenerateIf)),
            // contains additional data, can't be constructed here
            //5 => Some(Self::Block(BlockKind::GenerateFor)),
            6 => Some(Self::Block(BlockKind::Instance)),
            7 => Some(Self::Block(BlockKind::Package)),
            13 => Some(Self::Block(BlockKind::Process)),
            14 => Some(Self::Block(BlockKind::Generic)),
            15 => Some(Self::Special(SpecialKind::Eos)),
            16 => Some(Self::Signal(SignalKind::Signal)),
            17 => Some(Self::Signal(SignalKind::PortIn)),
            18 => Some(Self::Signal(SignalKind::PortOut)),
            19 => Some(Self::Signal(SignalKind::PortInout)),
            20 => Some(Self::Signal(SignalKind::PortBuffer)),
            21 => Some(Self::Signal(SignalKind::PortLinkage)),
            _ => None,
        }
    }

    fn from_u64(n: u64) -> Option<Self> {
        Self::from_i64(n.try_into().ok()?)
    }
}

impl Kind {
    pub fn parse(i: Input) -> IResult<Self> {
        map_res(u8, |n| {
            Self::from_u8(n).ok_or(format!("Unknown hierarchy kind with ID {n} ({n:#02x})"))
        })(i)
    }
}

/// A scalar part of a signal. Signals with scalar types refer to exactly one of these, while
/// composite signals may refer to multiple.
pub type SignalPart = RootScalar;

/// A hierarchy node, either a block or a signal.
#[derive(Clone, PartialEq, Debug)]
pub enum Node {
    Block(Block),
    Signal(Signal),
}

/// A hierarchy block, containing zero or more [`Node`]s.
#[derive(Clone, PartialEq, Debug)]
pub struct Block {
    pub name: Rc<str>,
    pub kind: BlockKind,
    pub children: Vec<Node>,
}

/// An index into the list of all [`SignalPart`]s. Equal `PartIndex` values refer to the same data.
pub type PartIndex = usize;

/// A signal containing references to one or more [`SignalPart`]s, depending on the signal's type.
#[derive(Clone, PartialEq, Debug)]
pub struct Signal {
    pub name: Rc<str>,
    pub kind: SignalKind,
    pub typ: Type,
    pub scalars: Vec<PartIndex>,
}
