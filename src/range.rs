use std::convert::TryInto;
use std::fmt;
use std::rc::Rc;

use nom::{
    combinator::{map, map_res},
    error::context,
    number::streaming::u8,
    sequence::tuple,
};
use num_traits::{
    ops::checked::{CheckedAdd, CheckedSub},
    FromPrimitive, PrimInt,
};

use crate::parse::{Endianness, IResult, Input};
use crate::types::Scalar;
use crate::RuntimeInterfaceKind;

/// Direction of a [`Range`], either `to` or `downto`.
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Direction {
    To,
    Downto,
}

impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::To => write!(f, "to"),
            Self::Downto => write!(f, "downto"),
        }
    }
}

/// A [`ScalarSubtype`](crate::types::ScalarSubtype)'s range, consisting of left and right bounds and a direction.
#[derive(Clone, PartialEq, Debug)]
pub struct Range<T: Scalar> {
    pub base: Rc<T>,
    pub left: T::ValueType,
    pub right: T::ValueType,
    pub direction: Direction,
}

impl<T: Scalar> Range<T> {
    pub fn parse(t: &Rc<T>, endianness: Endianness) -> impl Fn(Input) -> IResult<Range<T>> + '_ {
        move |i| {
            map(
                tuple((
                    map_res(context("range type and direction", u8), |tag| {
                        let expected_type = T::range_rti_kind();
                        let actual_type = RuntimeInterfaceKind::from_u8(tag & 0x7f)
                            .ok_or(format!("Unknown range type: {}", tag & 0x7f))?;
                        if actual_type != expected_type {
                            return Err(format!(
                                "Wrong range type: expected {expected_type:?}, got {actual_type:?}"
                            ));
                        };

                        if tag & 0x80 == 0 {
                            Ok(Direction::To)
                        } else {
                            Ok(Direction::Downto)
                        }
                    }),
                    context("left bound", |i| t.parse_value(endianness, i)),
                    context("right bound", |i| t.parse_value(endianness, i)),
                )),
                |(direction, left, right)| Range {
                    base: Rc::clone(t),
                    left,
                    right,
                    direction,
                },
            )(i)
        }
    }

    pub fn contains(&self, x: &T::ValueType) -> bool {
        match self.direction {
            Direction::To => &self.left <= x && x <= &self.right,
            Direction::Downto => &self.left >= x && x >= &self.right,
        }
    }
}

impl<T> Range<T>
where
    T: Scalar,
    T::ValueType: PrimInt + TryInto<usize>,
{
    /// Count the number of elements contained by the range. Returns [`None`] if the range is a
    /// null range.
    pub fn elements(&self) -> Option<usize> {
        match self.direction {
            Direction::To => self.right.checked_sub(&self.left)?,
            Direction::Downto => self.left.checked_sub(&self.right)?,
        }
        .checked_add(&num_traits::one())?
        .max(num_traits::zero())
        .try_into()
        .ok()
    }
}

impl<T: Scalar> fmt::Display for Range<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {} {}",
            self.base.fmt_value(self.left),
            self.direction,
            self.base.fmt_value(self.right)
        )
    }
}
