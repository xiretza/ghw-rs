#![deny(unsafe_code, non_ascii_idents)]
#![warn(clippy::pedantic, clippy::cargo)]
#![warn(clippy::as_conversions)]
// all parsers return `Result`s, no need to document them
#![allow(clippy::missing_errors_doc)]

use nom::combinator::map_res;
use nom::number::streaming::u8;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;

pub mod hierarchy;
pub mod parse;
pub mod range;
pub mod types;

use parse::{IResult, Input};

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq, Debug, FromPrimitive)]
pub enum RuntimeInterfaceKind {
    Top = 0,
    Library = 1,
    Package = 2,
    PackageBody = 3,
    Entity = 4,
    Architecture = 5,
    Process = 6,
    Block = 7,
    IfGenerate = 8,
    ForGenerate = 9,
    Instance = 10,
    Constant = 11,
    Iterator = 12,
    Variable = 13,
    Signal = 14,
    File = 15,
    Port = 16,
    Generic = 17,
    Alias = 18,
    Guard = 19,
    Component = 20,
    Attribute = 21,
    TypeB2 = 22,
    TypeE8 = 23,
    //TypeE32 = 24,
    TypeI32 = 25,
    TypeI64 = 26,
    TypeF64 = 27,
    TypeP32 = 28,
    TypeP64 = 29,
    //TypeAccess = 30,
    TypeArray = 31,
    TypeRecord = 32,
    //TypeFile = 33,
    SubtypeScalar = 34,
    SubtypeArray = 35,
    //SubtypeArrayPointer = 36,
    SubtypeUnboundedArray = 37,
    SubtypeRecord = 38,
    SubtypeUnboundedRecord = 39,
    Error = 40,
}

impl RuntimeInterfaceKind {
    pub fn parse(i: Input) -> IResult<Self> {
        map_res(u8, |n| {
            Self::from_u8(n).ok_or(format!("Unknown type kind with ID {n} ({n:#02x})"))
        })(i)
    }
}
