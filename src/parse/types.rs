use nom::{
    branch::alt,
    combinator::{map, map_res, success, verify},
    error::context,
    multi::length_count,
    sequence::{pair, preceded},
};
use nom_leb128::{leb128_i64, leb128_u32};
use num_traits::PrimInt;
use std::convert::{TryFrom, TryInto};
use std::marker::PhantomData;
use std::rc::Rc;

use super::{error_custom, foreach, GhwFile, IResult, Input};
use crate::range::Range;
use crate::types::{
    Array, ArrayOrSubtype, BoundedArraySubtype, BoundedRecordSubtype, Discrete, Enum, Number,
    Physical, PhysicalUnit, Record, RecordOrSubtype, Scalar, ScalarOrSubtype, ScalarSubtype, Type,
    UnboundedArraySubtype, UnboundedRecordSubtype,
};
use crate::RuntimeInterfaceKind;

#[derive(Clone, Copy, PartialEq, Debug)]
struct Context<'a> {
    pub file: &'a GhwFile,
    pub types: &'a [Type],
}

pub(crate) fn type_id(types: &[Type]) -> impl Fn(Input) -> IResult<Type> + '_ {
    move |i| {
        map_res(leb128_u32, |id| {
            let id: usize = id
                .try_into()
                .map_err(|e| format!("Type ID out of range: {e:?}"))?;
            types
                .get(id - 1)
                .ok_or(format!("No type with ID {id}"))
                .map(Type::clone)
        })(i)
    }
}

fn type_parser<'context, P: 'context>(
    ctx: Context<'context>,
    rti_kind: RuntimeInterfaceKind,
    parser: P,
) -> impl Fn(Input) -> IResult<Type> + 'context
where
    P: for<'input> Fn(Context, Option<Rc<str>>, Input<'input>) -> IResult<'input, Type>,
{
    move |i| {
        let (i, name) = preceded(
            context(
                "indicated type kind",
                verify(RuntimeInterfaceKind::parse, |&kind| kind == rti_kind),
            ),
            context("type name", ctx.file.string_id()),
        )(i)?;

        // can't use cut() because parser is FnOnce (moves `name`)
        parser(ctx, name, i).map_err(|e| match e {
            nom::Err::Error(e) => nom::Err::Failure(e),
            x => x,
        })
    }
}

fn type_parser_named<'context, P: 'context>(
    ctx: Context<'context>,
    rti_kind: RuntimeInterfaceKind,
    parser: P,
) -> impl Fn(Input) -> IResult<Type> + 'context
where
    P: for<'input> Fn(Context, Rc<str>, Input<'input>) -> IResult<'input, Type>,
{
    move |i| {
        let (i, name) = preceded(
            context(
                "indicated type kind",
                verify(RuntimeInterfaceKind::parse, |&kind| kind == rti_kind),
            ),
            context(
                "type name",
                map_res(ctx.file.string_id(), |name| {
                    name.ok_or_else(|| format!("{rti_kind:?} type can not be anonymous"))
                }),
            ),
        )(i)?;

        // can't use cut() because parser is FnOnce (moves `name`)
        parser(ctx, name, i).map_err(|e| match e {
            nom::Err::Error(e) => nom::Err::Failure(e),
            x => x,
        })
    }
}

#[allow(clippy::needless_pass_by_value)]
fn r#enum<'input>(ctx: Context<'_>, name: Rc<str>, i: Input<'input>) -> IResult<'input, Type> {
    map_res(
        length_count(
            context("number of enum variants", leb128_u32),
            context(
                "enum variant name",
                map_res(ctx.file.string_id(), |name| {
                    name.ok_or_else(|| "Enum variant must have a name".to_string())
                }),
            ),
        ),
        |variants| -> Result<_, String> {
            Ok(Type::Enum(ScalarOrSubtype::Root(Rc::new(Enum {
                name: Rc::clone(&name),
                variants,
            }))))
        },
    )(i)
}

#[allow(clippy::needless_pass_by_value)]
fn physical_inner<'input>(
    ctx: Context<'_>,
    name: &str,
    i: Input<'input>,
) -> IResult<'input, (Rc<str>, Vec<PhysicalUnit>)> {
    map_res(
        length_count(
            context("number of units", verify(leb128_u32, |&x| x >= 1)),
            pair(
                context(
                    "unit name",
                    map_res(ctx.file.string_id(), |name| {
                        name.ok_or_else(|| "Unit must have a name".to_string())
                    }),
                ),
                context("unit value", leb128_i64),
            ),
        ),
        |units| {
            let mut units = units
                .into_iter()
                .map(|(name, value)| PhysicalUnit { name, value });

            // guaranteed to be at least 1 long by parser above
            let primary = units.next().unwrap();
            if primary.value != 1 {
                return Err(format!(
                    "Primary unit of physical type {name} is equal to a multiple of itself"
                ));
            }

            Ok((primary.name, units.collect()))
        },
    )(i)
}

fn physical_32<'input>(ctx: Context<'_>, name: Rc<str>, i: Input<'input>) -> IResult<'input, Type> {
    let (i, (primary_unit, secondary_units)) = physical_inner(ctx, &name, i)?;

    Ok((
        i,
        Type::P32(ScalarOrSubtype::Root(Rc::new(Physical {
            name,
            primary_unit,
            secondary_units,
            _phantom: PhantomData,
        }))),
    ))
}

fn physical_64<'input>(ctx: Context<'_>, name: Rc<str>, i: Input<'input>) -> IResult<'input, Type> {
    let (i, (primary_unit, secondary_units)) = physical_inner(ctx, &name, i)?;

    Ok((
        i,
        Type::P64(ScalarOrSubtype::Root(Rc::new(Physical {
            name,
            primary_unit,
            secondary_units,
            _phantom: PhantomData,
        }))),
    ))
}

fn discrete_subtype_bounds_inner<'input, T>(
    ctx: Context<'_>,
    base: ScalarOrSubtype<T>,
    i: Input<'input>,
) -> IResult<'input, (Option<usize>, ScalarOrSubtype<T>)>
where
    T: Scalar,
    T::ValueType: PrimInt + TryInto<usize>,
{
    let (i, range) = Range::<T>::parse(base.root(), ctx.file.data_layout.endianness)(i)?;
    Ok((
        i,
        (
            range.elements(),
            ScalarOrSubtype::Subtype(Rc::new(ScalarSubtype {
                name: None,
                base,
                range,
            })),
        ),
    ))
}

fn discrete_subtype_bounds<'input>(
    ctx: Context<'_>,
    base: &Discrete,
    i: Input<'input>,
) -> IResult<'input, (Option<usize>, Discrete)> {
    match base {
        Discrete::Enum(base) => context(
            "enumeration subtype",
            map(
                |i| discrete_subtype_bounds_inner(ctx, base.clone(), i),
                |(els, t)| (els, Discrete::Enum(t)),
            ),
        )(i),
        Discrete::I32(base) => context(
            "32-bit integer subtype",
            map(
                |i| discrete_subtype_bounds_inner(ctx, base.clone(), i),
                |(els, t)| (els, Discrete::I32(t)),
            ),
        )(i),
        Discrete::P32(base) => context(
            "32-bit physical subtype",
            map(
                |i| discrete_subtype_bounds_inner(ctx, base.clone(), i),
                |(els, t)| (els, Discrete::P32(t)),
            ),
        )(i),
        Discrete::P64(base) => context(
            "64-bit physical subtype",
            map(
                |i| discrete_subtype_bounds_inner(ctx, base.clone(), i),
                |(els, t)| (els, Discrete::P64(t)),
            ),
        )(i),
    }
}

fn scalar_subtype_bounds_inner<'input, T: Scalar>(
    ctx: Context<'_>,
    base: ScalarOrSubtype<T>,
    name: Option<Rc<str>>,
    i: Input<'input>,
) -> IResult<'input, ScalarOrSubtype<T>> {
    let (i, range) = Range::<T>::parse(base.root(), ctx.file.data_layout.endianness)(i)?;

    Ok((
        i,
        ScalarOrSubtype::Subtype(Rc::new(ScalarSubtype { name, base, range })),
    ))
}

#[allow(clippy::needless_pass_by_value)]
fn scalar_subtype<'input>(
    ctx: Context<'_>,
    name: Option<Rc<str>>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    let (i, base) = context("base type", type_id(ctx.types))(i)?;

    match base {
        Type::Enum(base) => context(
            "enumeration subtype",
            map(
                |i| scalar_subtype_bounds_inner(ctx, base.clone(), name.clone(), i),
                Type::Enum,
            ),
        )(i),
        Type::I32(base) => context(
            "32-bit integer subtype",
            map(
                |i| scalar_subtype_bounds_inner(ctx, base.clone(), name.clone(), i),
                Type::I32,
            ),
        )(i),
        Type::F64(base) => context(
            "64-bit floating point subtype",
            map(
                |i| scalar_subtype_bounds_inner(ctx, base.clone(), name.clone(), i),
                Type::F64,
            ),
        )(i),
        Type::P32(base) => context(
            "32-bit physical subtype",
            map(
                |i| scalar_subtype_bounds_inner(ctx, base.clone(), name.clone(), i),
                Type::P32,
            ),
        )(i),
        Type::P64(base) => context(
            "64-bit physical subtype",
            map(
                |i| scalar_subtype_bounds_inner(ctx, base.clone(), name.clone(), i),
                Type::P64,
            ),
        )(i),
        Type::Array(_) | Type::Record(_) => {
            error_custom(i, format!("Unhandled base type {base:?}"))
        }
    }
}

#[allow(clippy::needless_pass_by_value)]
fn array<'input>(ctx: Context<'_>, name: Rc<str>, i: Input<'input>) -> IResult<'input, Type> {
    map(
        pair(
            context("element type", type_id(ctx.types)),
            length_count(
                context("number of dimensions", leb128_u32),
                context(
                    "array dimension",
                    map_res(type_id(ctx.types), Discrete::try_from),
                ),
            ),
        ),
        |(element, dimensions)| {
            Type::Array(ArrayOrSubtype::Root(Rc::new(Array {
                name: Rc::clone(&name),
                element,
                dimensions,
            })))
        },
    )(i)
}

#[allow(clippy::shadow_unrelated)]
fn array_subtype_bounds<'input>(
    ctx: Context<'_>,
    base: ArrayOrSubtype,
    name: Option<Rc<str>>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    let base_arr = base.root();

    let mut number_elements = 1;

    let mut dimensions = Vec::with_capacity(base_arr.dimensions.len());
    let mut input = i;
    for dim in &base_arr.dimensions {
        let i = input;
        let (i, (elements, typ)) =
            context("index constraint", |i| discrete_subtype_bounds(ctx, dim, i))(i)?;
        input = i;
        dimensions.push(typ);
        number_elements *= elements.expect("array indices must be bounded");
    }
    let i = input;

    let (i, element) = match base_arr.element.number_scalars() {
        None => composite_subtype_bounds(ctx, base_arr.element.clone(), None, i),
        Some(_) => Ok((i, base_arr.element.clone())),
    }?;

    debug_assert!(element.number_scalars().is_some());

    Ok((
        i,
        Type::Array(ArrayOrSubtype::BoundedSubtype(Rc::new(
            BoundedArraySubtype {
                name,
                base,
                element,
                dimensions,
                number_elements,
            },
        ))),
    ))
}

fn bounded_array_subtype<'input>(
    ctx: Context<'_>,
    name: Option<Rc<str>>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    let (i, base) = context(
        "base type",
        map_res(type_id(ctx.types), ArrayOrSubtype::try_from),
    )(i)?;
    array_subtype_bounds(ctx, base, name, i)
}

#[allow(clippy::needless_pass_by_value)]
fn unbounded_array_subtype<'input>(
    ctx: Context<'_>,
    name: Rc<str>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    map(
        context(
            "base type",
            map_res(type_id(ctx.types), ArrayOrSubtype::try_from),
        ),
        |base| {
            Type::Array(ArrayOrSubtype::UnboundedSubtype(Rc::new(
                UnboundedArraySubtype {
                    name: Rc::clone(&name),
                    base,
                },
            )))
        },
    )(i)
}

#[allow(clippy::needless_pass_by_value)]
fn record<'input>(ctx: Context<'_>, name: Rc<str>, i: Input<'input>) -> IResult<'input, Type> {
    map(
        length_count(
            context("number of fields", leb128_u32),
            pair(
                context(
                    "field name",
                    map_res(ctx.file.string_id(), |name| {
                        name.ok_or("Record field must have a name")
                    }),
                ),
                context("field type", type_id(ctx.types)),
            ),
        ),
        |fields| {
            Type::Record(RecordOrSubtype::Root(Rc::new(Record {
                name: Rc::clone(&name),
                fields,
            })))
        },
    )(i)
}

fn record_subtype_bounds<'input>(
    ctx: Context<'_>,
    base: RecordOrSubtype,
    name: Option<Rc<str>>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    let (i, fields) = foreach(
        base.field_types(),
        |base_field, i| match base_field.number_scalars() {
            Some(_) => Ok((i, base_field.clone())),
            None => composite_subtype_bounds(ctx, base_field.clone(), None, i),
        },
        i,
    )?;

    Ok((
        i,
        Type::Record(RecordOrSubtype::BoundedSubtype(Rc::new(
            BoundedRecordSubtype { name, base, fields },
        ))),
    ))
}

fn bounded_record_subtype<'input>(
    ctx: Context<'_>,
    name: Option<Rc<str>>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    let (i, base) = context(
        "base type",
        map_res(type_id(ctx.types), RecordOrSubtype::try_from),
    )(i)?;
    record_subtype_bounds(ctx, base, name, i)
}

fn unbounded_record_subtype<'input>(
    ctx: Context<'_>,
    name: Rc<str>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    let (i, base) = context(
        "base type",
        map_res(type_id(ctx.types), RecordOrSubtype::try_from),
    )(i)?;

    Ok((
        i,
        Type::Record(RecordOrSubtype::UnboundedSubtype(Rc::new(
            UnboundedRecordSubtype { name, base },
        ))),
    ))
}

fn composite_subtype_bounds<'input>(
    ctx: Context<'_>,
    base: Type,
    name: Option<Rc<str>>,
    i: Input<'input>,
) -> IResult<'input, Type> {
    match base {
        Type::Array(base) => array_subtype_bounds(ctx, base, name, i),
        Type::Record(base) => record_subtype_bounds(ctx, base, name, i),
        Type::Enum(_) | Type::I32(_) | Type::F64(_) | Type::P32(_) | Type::P64(_) => error_custom(
            i,
            format!("Tried to get composite subtype's bounds, but base type is scalar: {base}"),
        ),
    }
}

pub fn parse_type<'input>(
    file: &GhwFile,
    types: &[Type],
    i: Input<'input>,
) -> IResult<'input, Type> {
    let ctx = Context { file, types };

    alt((
        context(
            "enumeration type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeE8, r#enum),
        ),
        context(
            "2-value enumeration type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeB2, r#enum),
        ),
        context(
            "32-bit signed integer type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeI32, |_ctx, name, i| {
                success(Type::I32(ScalarOrSubtype::Root(Rc::new(Number {
                    name,
                    _phantom: PhantomData,
                }))))(i)
            }),
        ),
        context(
            "64-bit floating point type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeF64, |_ctx, name, i| {
                success(Type::F64(ScalarOrSubtype::Root(Rc::new(Number {
                    name,
                    _phantom: PhantomData,
                }))))(i)
            }),
        ),
        context(
            "32-bit physical type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeP32, physical_32),
        ),
        context(
            "64-bit physical type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeP64, physical_64),
        ),
        context(
            "scalar subtype",
            type_parser(ctx, RuntimeInterfaceKind::SubtypeScalar, scalar_subtype),
        ),
        context(
            "array type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeArray, array),
        ),
        context(
            "bounded array subtype",
            type_parser(
                ctx,
                RuntimeInterfaceKind::SubtypeArray,
                bounded_array_subtype,
            ),
        ),
        context(
            "unbounded array subtype",
            type_parser_named(
                ctx,
                RuntimeInterfaceKind::SubtypeUnboundedArray,
                unbounded_array_subtype,
            ),
        ),
        context(
            "record type",
            type_parser_named(ctx, RuntimeInterfaceKind::TypeRecord, record),
        ),
        context(
            "bounded record subtype",
            type_parser(
                ctx,
                RuntimeInterfaceKind::SubtypeRecord,
                bounded_record_subtype,
            ),
        ),
        context(
            "unbounded record subtype",
            type_parser_named(
                ctx,
                RuntimeInterfaceKind::SubtypeUnboundedRecord,
                unbounded_record_subtype,
            ),
        ),
        // TODO: implement other types
        context(
            "unhandled type",
            map_res(
                pair(
                    context("indicated type kind", RuntimeInterfaceKind::parse),
                    context("type name", ctx.file.string_id()),
                ),
                |(kind, name)| {
                    Err(format!(
                        "Unhandled type kind {:?} (type name {})",
                        kind,
                        name.as_deref().unwrap_or("unknown")
                    ))
                },
            ),
        ),
    ))(i)
}
