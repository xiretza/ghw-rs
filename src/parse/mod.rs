use nom::{
    bytes::streaming::tag,
    combinator::{map, map_opt, map_res, verify},
    error::context,
    number::streaming::{i32, i64, u8},
    sequence::{pair, preceded, tuple},
};
use nom_leb128::leb128_u32;
use std::convert::TryFrom;
use std::fmt;
use std::rc::Rc;

use crate::types::Type;

pub mod sections;
pub mod types;

use sections::{
    cycle, directory, hierarchy_section, snapshot, string_section, trailer, type_section,
    wkt_section,
};

#[derive(PartialEq, Eq, Debug)]
pub enum ErrorKind {
    Nom(nom::error::ErrorKind),
    Custom(String),
    Context(&'static str),
    External(nom::error::ErrorKind, String),
}

impl From<nom::error::ErrorKind> for ErrorKind {
    fn from(kind: nom::error::ErrorKind) -> Self {
        Self::Nom(kind)
    }
}

#[derive(PartialEq, Eq)]
pub struct Error<I> {
    pub errors: Vec<(I, ErrorKind)>,
}

impl<'a> fmt::Debug for Error<&'a [u8]> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        writeln!(f)?;

        for (input, kind) in self.errors.iter().rev() {
            let message = match kind {
                ErrorKind::Nom(k) => format!("in {k:?} combinator:"),
                ErrorKind::Custom(s) => format!("{s}:"),
                ErrorKind::Context(s) => format!("in {s}:"),
                ErrorKind::External(_, s) => format!("external error: {s}"),
            };

            writeln!(f, "{message}")?;

            let len = input.len().min(10);
            let shown_input = &input[..len];
            for b in shown_input {
                write!(f, " {b:02x}")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl<I> Error<I> {
    pub fn append_kind(input: I, kind: ErrorKind, mut other: Self) -> Self {
        other.errors.push((input, kind));
        other
    }

    pub fn with_kind(input: I, kind: ErrorKind) -> Self {
        Self {
            errors: vec![(input, kind)],
        }
    }

    pub fn custom(input: I, message: String) -> Self {
        Self::with_kind(input, ErrorKind::Custom(message))
    }
}

impl<I> nom::error::ParseError<I> for Error<I> {
    fn from_error_kind(input: I, kind: nom::error::ErrorKind) -> Self {
        Self::with_kind(input, kind.into())
    }

    fn append(input: I, kind: nom::error::ErrorKind, other: Self) -> Self {
        Self::append_kind(input, kind.into(), other)
    }
}

impl<I> nom::error::ContextError<I> for Error<I> {
    fn add_context(input: I, ctx: &'static str, other: Self) -> Self {
        Self::append_kind(input, ErrorKind::Context(ctx), other)
    }
}

impl<I, E: std::string::ToString> nom::error::FromExternalError<I, E> for Error<I> {
    fn from_external_error(input: I, kind: nom::error::ErrorKind, e: E) -> Self {
        Self::with_kind(input, ErrorKind::External(kind, e.to_string()))
    }
}

pub type Input<'a> = &'a [u8];
pub type IResult<'a, T> = nom::IResult<Input<'a>, T, Error<Input<'a>>>;

pub fn error<T>(err: Error<Input>) -> IResult<T> {
    Err(nom::Err::Error(err))
}

pub fn error_custom<T>(input: Input, message: String) -> IResult<T> {
    error(Error::custom(input, message))
}

fn foreach<I, O, E, Iter, T, F>(
    iterator: Iter,
    mut parser: F,
    input: I,
) -> nom::IResult<I, Vec<O>, E>
where
    Iter: Iterator<Item = T>,
    F: FnMut(T, I) -> nom::IResult<I, O, E>,
{
    let mut result = Vec::with_capacity(iterator.size_hint().0);
    let mut input = input;

    for item in iterator {
        let (i, element) = parser(item, input)?;

        input = i;
        result.push(element);
    }

    Ok((input, result))
}

/// Same as [`nom::combinator::flat_map`], but returns both parsers' outputs (`O1` has to be [`Clone`]).
fn flat_map_both<I, O1, O2, E, P1, P2, F>(
    mut parser1: P1,
    mut make_parser2: F,
) -> impl FnMut(I) -> nom::IResult<I, (O1, O2), E>
where
    P1: nom::Parser<I, O1, E>,
    O1: Clone,
    P2: nom::Parser<I, O2, E>,
    F: FnMut(O1) -> P2,
    E: nom::error::ParseError<I>,
{
    move |i| {
        let (i, x) = parser1.parse(i)?;
        let (i, y) = make_parser2(x.clone()).parse(i)?;
        Ok((i, (x, y)))
    }
}

#[derive(PartialEq, Debug)]
pub struct GhwFile {
    pub version: (u8, u8),
    pub data_layout: DataLayout,
    pub strings: sections::StringSection,
    pub types: sections::TypeSection,
    pub well_known_types: sections::WellKnownTypesSection,
    pub hierarchy: sections::HierarchySection,
    pub snapshot: sections::Snapshot,
    pub deltas: Vec<sections::DeltaStep>,
}

impl GhwFile {
    pub fn parse(i: Input) -> IResult<Self> {
        let (i, mut file) = map(Self::parse_header, |(version, data_layout)| Self {
            version,
            data_layout,
            strings: Vec::new(),
            types: Vec::new(),
            well_known_types: sections::WellKnownTypesSection::default(),
            hierarchy: sections::HierarchySection::default(),
            snapshot: sections::Snapshot {
                time: 0,
                values: Vec::new(),
            },
            deltas: Vec::new(),
        })(i)?;

        let (i, strings) = context("string section", string_section(&file))(i)?;
        file.strings = strings;

        let (i, types) = context("type section", type_section(&file))(i)?;
        file.types = types;

        let (i, wkts) = context("well-known types section", wkt_section(&file))(i)?;
        file.well_known_types = wkts;

        let (i, hierarchy) = context("hierarchy section", hierarchy_section(&file))(i)?;
        file.hierarchy = hierarchy;

        let (i, (snapshot, deltas)) = preceded(
            context("end of file header", tag("EOH\0")),
            pair(
                context("signal snapshot", snapshot(&file)),
                context("delta cycles", cycle(&file)),
            ),
        )(i)?;
        file.snapshot = snapshot;
        file.deltas = deltas;

        let file = file;

        let (i, (_directory, _trailer)) = tuple((
            context("section directory", directory(&file)),
            context("trailer", trailer(&file)),
        ))(i)?;

        Ok((i, file))
    }

    pub fn parse_header(i: Input) -> IResult<((u8, u8), DataLayout)> {
        preceded(
            pair(
                context("Magic", tag("GHDLwave\n")),
                context("Header length", verify(u8, |&x| x == 16)),
            ),
            pair(
                context(
                    "File version",
                    verify(pair(u8, u8), |&(major, minor)| major == 0 && minor <= 1),
                ),
                DataLayout::parse,
            ),
        )(i)
    }

    /// Parse an i32 with the endianness defined in the file header.
    pub fn i32<'input>(&self) -> impl Fn(Input<'input>) -> IResult<'input, i32> {
        i32(self.data_layout.endianness.into())
    }

    /// Parse an i64 with the endianness defined in the file header.
    pub fn i64<'input>(&self) -> impl Fn(Input<'input>) -> IResult<'input, i64> {
        i64(self.data_layout.endianness.into())
    }

    /// Parse a string ID and return the corresponding string. ID 0 corresponds to no name.
    pub fn string_id(&self) -> impl Fn(Input) -> IResult<Option<Rc<str>>> + '_ {
        move |i| {
            map_res(leb128_u32, |n| -> Result<_, String> {
                let n = usize::try_from(n).map_err(|_| format!("Invalid string ID: {n}"))?;

                if n == 0 {
                    Ok(None)
                } else {
                    Ok(Some(Rc::clone(
                        self.strings
                            .get(n - 1)
                            .ok_or(format!("No string with ID {n}"))?,
                    )))
                }
            })(i)
        }
    }

    /// Parse a type ID and return the corresponding type.
    pub fn type_id(&self) -> impl Fn(Input) -> IResult<Type> + '_ {
        types::type_id(&self.types)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Endianness {
    Little,
    Big,
}

impl From<Endianness> for nom::number::Endianness {
    fn from(e: Endianness) -> nom::number::Endianness {
        match e {
            Endianness::Little => nom::number::Endianness::Little,
            Endianness::Big => nom::number::Endianness::Big,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct DataLayout {
    pub endianness: Endianness,
    pub word_length: usize,
    pub offset_length: usize,
}

impl DataLayout {
    pub fn parse(i: Input) -> IResult<DataLayout> {
        map(
            tuple((
                context(
                    "Word endianness",
                    map_opt(u8, |x| match x {
                        1 => Some(Endianness::Little),
                        2 => Some(Endianness::Big),
                        _ => None,
                    }),
                ),
                context("Word length", verify(u8, |&l| l == 4)),
                context("Offset length", verify(u8, |&l| l == 1)),
                context("Reserved", tag([0])),
            )),
            |(endianness, word_length, offset_length, _)| DataLayout {
                endianness,
                word_length: word_length.into(),
                offset_length: offset_length.into(),
            },
        )(i)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_header() {
        let (tail, (version, layout)) =
            GhwFile::parse_header(b"GHDLwave\n\x10\x00\x01\x01\x04\x01\x00").unwrap();

        assert!(tail.is_empty());
        assert_eq!(version, (0, 1));
        assert_eq!(
            layout,
            DataLayout {
                endianness: Endianness::Little,
                word_length: 4,
                offset_length: 1,
            }
        );
    }
}
