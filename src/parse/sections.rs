use log::{debug, trace};
use nom::{
    bytes::streaming::{tag, take, take_till1, take_while},
    combinator::{cut, flat_map, map, map_opt, map_res, recognize, value, verify},
    error::context,
    multi::{count, length_count, many0, many_till},
    number::streaming::u8,
    sequence::{delimited, pair, preceded, terminated, tuple},
};
use nom_leb128::{leb128_i64, leb128_u32};
use num_traits::FromPrimitive;
use std::cmp::Ordering;
use std::convert::{TryFrom, TryInto};
use std::mem;
use std::rc::Rc;

use super::{error_custom, flat_map_both, DataLayout, Error, GhwFile, IResult, Input};
use crate::hierarchy::{self, Block, BlockKind, Node, PartIndex, Signal, SignalPart, SpecialKind};
use crate::parse::types::parse_type;
use crate::types::{ArrayOrSubtype, ParseValue, RootScalar, ScalarValue, Type};

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum SectionKind {
    Strings,
    Types,
    WellKnownTypes,
    Hierarchy,
    EndOfHeader,
    Snapshot,
    Cycle,
    Directory,
    Trailer,
}

impl SectionKind {
    #[must_use]
    pub fn id(self) -> &'static str {
        match self {
            Self::Strings => "STR",
            Self::Types => "TYP",
            Self::WellKnownTypes => "WKT",
            Self::Hierarchy => "HIE",
            Self::EndOfHeader => "EOH",
            Self::Snapshot => "SNP",
            Self::Cycle => "CYC",
            Self::Directory => "DIR",
            Self::Trailer => "TAI",
        }
    }

    #[must_use]
    pub fn from_id(id: &[u8]) -> Option<Self> {
        match id {
            b"STR" => Some(Self::Strings),
            b"TYP" => Some(Self::Types),
            b"WKT" => Some(Self::WellKnownTypes),
            b"HIE" => Some(Self::Hierarchy),
            b"EOH" => Some(Self::EndOfHeader),
            b"SNP" => Some(Self::Snapshot),
            b"CYC" => Some(Self::Cycle),
            b"DIR" => Some(Self::Directory),
            b"TAI" => Some(Self::Trailer),
            _ => None,
        }
    }

    pub fn parse(self) -> impl FnMut(Input) -> IResult<Self> {
        move |i| value(self, terminated(tag(self.id()), tag("\0")))(i)
    }

    pub fn parse_any(i: Input) -> IResult<Self> {
        map_res(terminated(take(3_u32), tag("\0")), |id| {
            Self::from_id(id).ok_or_else(|| {
                format!(
                    "Unknown section ID: {} ({:?})",
                    String::from_utf8_lossy(id),
                    id
                )
            })
        })(i)
    }
}

fn section<'context, P: 'context, S>(
    file: &'context GhwFile,
    kind: SectionKind,
    parser: P,
) -> impl Fn(Input) -> IResult<S> + 'context
where
    P: for<'input> Fn(&GhwFile, Input<'input>) -> IResult<'input, S>,
{
    move |i| {
        preceded(
            context("section header", kind.parse()),
            cut(|i| parser(file, i)),
        )(i)
    }
}

fn latin1_to_string(s: &[u8]) -> String {
    s.iter().map(|&c| char::from(c)).collect()
}

/// Build a string by reusing a prefix of a previous string
///
/// The prefix and suffix are concatenated and the first [`next_prefix_len`] characters of
/// the resulting string are stored back to the prefix.
fn build_prefix_string<'a>(
    prefix: &mut Vec<u8>,
    suffix: &'a [u8],
    next_prefix_len: usize,
) -> Result<String, Error<&'a [u8]>> {
    let mut s = Vec::new();
    mem::swap(prefix, &mut s);

    s.extend_from_slice(suffix);

    let next_prefix = s
        .get(..next_prefix_len)
        .ok_or_else(|| {
            Error::custom(
                suffix,
                format!(
                    "Tried to reuse more than length of current string: length {}, reuse {}",
                    s.len(),
                    next_prefix_len
                ),
            )
        })?
        .to_vec();

    let s = latin1_to_string(&s);

    trace!(
        "Built string: {}, keeping first {} bytes ({:?}) for next",
        s,
        next_prefix_len,
        next_prefix,
    );
    *prefix = next_prefix;

    Ok(s)
}

pub type StringSection = Vec<Rc<str>>;

fn string_section_inner<'input>(
    file: &GhwFile,
    i: Input<'input>,
) -> IResult<'input, StringSection> {
    let (i, (num_strings, str_size)) = preceded(
        context("must be zero", tag("\0\0\0\0")),
        tuple((
            context("Number of strings", map_res(file.i32(), usize::try_from)),
            context("Total size of all strings", file.i32()),
        )),
    )(i)?;
    debug!("Building {} strings, total size {}", num_strings, str_size);

    // Encoding similar to ULEB128, but only 5 bits per byte to allow mixing with ASCII text
    let variable_length_int = map(
        recognize(pair(take_while(|c| c >= 128), u8)),
        |bytes: &[u8]| -> usize {
            let n = bytes
                .iter()
                .enumerate()
                .map(|(shift, &n)| (usize::from(n) & 0x1f) << (shift * 5))
                .sum();
            trace!("Reuse length transformation: {:?} becomes {}", bytes, n);
            n
        },
    );

    let (i, parts) = terminated(
        count(
            pair(
                context(
                    "Suffix of new string",
                    take_till1(|c| (0..=31).contains(&c) || (128..=159).contains(&c)),
                ),
                context(
                    "Number of characters to copy to next string",
                    variable_length_int,
                ),
            ),
            num_strings,
        ),
        context("End of string section", tag("EOS\0")),
    )(i)?;

    let strings: Vec<Rc<str>> = parts
        .iter()
        .scan(Vec::new(), |prefix, (suffix, next_prefix_len)| {
            Some(build_prefix_string(prefix, suffix, *next_prefix_len).map(Rc::from))
        })
        .collect::<Result<_, _>>()
        .map_err(nom::Err::Error)?;

    debug_assert_eq!(strings.len(), num_strings);
    debug_assert_eq!(
        strings.iter().map(|s| s.chars().count()).sum::<usize>(),
        str_size.try_into().unwrap()
    );

    Ok((i, strings))
}

pub fn string_section(file: &GhwFile) -> impl Fn(Input) -> IResult<StringSection> + '_ {
    section(file, SectionKind::Strings, string_section_inner)
}

pub type TypeSection = Vec<Type>;

fn type_section_inner<'input>(file: &GhwFile, i: Input<'input>) -> IResult<'input, TypeSection> {
    let (i, num_types): (_, usize) = preceded(
        context("must be zero", tag("\0\0\0\0")),
        map_res(context("number of types", file.i32()), usize::try_from),
    )(i)?;
    debug!("Building {} types", num_types);

    let mut types = Vec::with_capacity(num_types);

    let (i, _) = pair(
        count(
            context("type table entry", |i| {
                let (i, typ) = parse_type(file, &types, i)?;

                trace!("Type {}: {}", types.len(), typ);
                types.push(typ);

                Ok((i, ()))
            }),
            num_types,
        ),
        context("end of type section", tag("\0")),
    )(i)?;

    Ok((i, types))
}

pub fn type_section(file: &GhwFile) -> impl Fn(Input) -> IResult<TypeSection> + '_ {
    section(file, SectionKind::Types, type_section_inner)
}

#[non_exhaustive]
#[derive(Clone, PartialEq, Default, Debug)]
pub struct WellKnownTypesSection {
    pub boolean: Option<Type>,
    pub bit: Option<Type>,
    pub std_ulogic: Option<Type>,
}

fn wkt_section_inner<'input>(
    file: &GhwFile,
    i: Input<'input>,
) -> IResult<'input, WellKnownTypesSection> {
    let mut wkt = WellKnownTypesSection {
        boolean: None,
        bit: None,
        std_ulogic: None,
    };

    let (i, _) = preceded(
        context("must be zero", tag("\0\0\0\0")),
        many_till(
            map_res(
                pair(
                    context("well-known type index", u8),
                    context("affected type", file.type_id()),
                ),
                |(wkt_id, typ)| {
                    match wkt_id {
                        1 => {
                            debug!("boolean is {}", typ);
                            wkt.boolean = Some(typ);
                        }
                        2 => {
                            debug!("bit is {}", typ);
                            wkt.bit = Some(typ);
                        }
                        3 => {
                            debug!("std_ulogic is {}", typ);
                            wkt.std_ulogic = Some(typ);
                        }
                        _ => {
                            return Err(format!(
                                "Invalid well known type index: {wkt_id} (for {typ})"
                            ))
                        }
                    };
                    Ok(())
                },
            ),
            context("end of well-known types", tag("\0")),
        ),
    )(i)?;

    Ok((i, wkt))
}

pub fn wkt_section(file: &GhwFile) -> impl Fn(Input) -> IResult<WellKnownTypesSection> + '_ {
    section(file, SectionKind::WellKnownTypes, wkt_section_inner)
}

#[derive(Clone, PartialEq, Debug, Default)]
pub struct HierarchySection {
    pub scalars: Vec<SignalPart>,
    pub design: Vec<Node>,
}

fn signal_parts<'input>(
    file: &GhwFile,
    scalars: &mut Vec<SignalPart>,
    typ: &Type,
    i: Input<'input>,
) -> IResult<'input, Vec<PartIndex>> {
    if let Ok(scalar) = RootScalar::try_from(typ.root()) {
        let (i, part_num) = context(
            "signal part number",
            map_opt(leb128_u32, |n| {
                // signal parts are encoded one-indexed
                usize::try_from(n).ok()?.checked_sub(1)
            }),
        )(i)?;
        let parts_len = scalars.len();

        match part_num.cmp(&parts_len) {
            Ordering::Equal => scalars.push(scalar),
            Ordering::Greater => {
                return error_custom(
                    i,
                    format!(
                    "Signal part was defined out of order: {parts_len} parts defined, new part number {part_num}"
                ),
                )
            }
            Ordering::Less => {
                let prev = &scalars[part_num];

                if prev != &scalar {
                    return error_custom(
                        i,
                        format!(
                            "Incompatible signal part redefinition: part {part_num} was {prev}, now {scalar}"
                        ),
                    );
                }
            }
        }

        Ok((i, vec![part_num]))
    } else {
        match typ {
            Type::Enum(_) | Type::I32(_) | Type::F64(_) | Type::P32(_) | Type::P64(_) => {
                // already covered by the conversion to scalar above
                unreachable!()
            }
            Type::Array(ArrayOrSubtype::BoundedSubtype(arr)) => {
                map(
                    count(
                        context("array element signal", |i| {
                            signal_parts(file, scalars, &arr.element, i)
                        }),
                        arr.number_elements,
                    ),
                    // TODO: make this zero-copy
                    |children_nums| children_nums.into_iter().flatten().collect(),
                )(i)
            }
            Type::Array(ArrayOrSubtype::Root(_) | ArrayOrSubtype::UnboundedSubtype(_)) => {
                error_custom(i, "Signals must be bounded".to_owned())
            }
            Type::Record(r) => {
                let mut indices = Vec::new();
                let mut i = i;

                for typ in r.field_types() {
                    let (tail, child_indices) = signal_parts(file, scalars, typ, i)?;
                    i = tail;
                    // TODO: make this zero-copy
                    indices.extend_from_slice(&child_indices);
                }

                Ok((i, indices))
            }
        }
    }
}

fn hierarchy_node<'input>(
    file: &GhwFile,
    scalars: &mut Vec<SignalPart>,
    i: Input<'input>,
) -> IResult<'input, Node> {
    use crate::hierarchy::Kind;

    let i_kind = i;
    let (i, (kind_id, name)) = pair(
        context("node kind", u8),
        context(
            "node name",
            map_res(file.string_id(), |name| {
                name.ok_or("Hierarchy nodes must have a name")
            }),
        ),
    )(i)?;

    let (i, kind) = match Kind::from_u8(kind_id) {
        Some(k) => Ok((i, k)),
        None if kind_id == 5 => context(
            "generate-for iteration count",
            map(
                flat_map_both(
                    context(
                        "iteration type",
                        map_res(file.type_id(), |typ| RootScalar::try_from(typ.root())),
                    ),
                    |iter_typ| {
                        context("iteration value", move |i| {
                            iter_typ.clone().parse_value(file.data_layout.endianness, i)
                        })
                    },
                ),
                |(iterator_type, iterator_value)| {
                    Kind::Block(BlockKind::GenerateFor {
                        iterator_type,
                        iterator_value,
                    })
                },
            ),
        )(i),
        None => error_custom(
            i_kind,
            format!("Unknown hierarchy kind with ID {kind_id} ({kind_id:#02x})"),
        ),
    }?;

    match kind {
        Kind::Special(_) | Kind::Block(BlockKind::Design) => {
            error_custom(i_kind, format!("Invalid hierarchy node kind: {kind:?}"))
        }
        Kind::Block(BlockKind::Process) => {
            trace!("Built process {}", name);
            Ok((
                i,
                Node::Block(Block {
                    name,
                    kind: BlockKind::Process,
                    children: Vec::new(),
                }),
            ))
        }
        Kind::Block(kind) => {
            let (i, children) = terminated(
                many0(context("child node", |i| hierarchy_node(file, scalars, i))),
                context(
                    "end of block",
                    verify(Kind::parse, |k| *k == Kind::Special(SpecialKind::Eos)),
                ),
            )(i)?;

            trace!("Built {:?} {}", kind, name);
            Ok((
                i,
                Node::Block(Block {
                    name,
                    kind,
                    children,
                }),
            ))
        }
        Kind::Signal(kind) => {
            let (i, typ) = context("signal type", file.type_id())(i)?;

            let (i, scalars) =
                context("signal parts", |i| signal_parts(file, scalars, &typ, i))(i)?;

            trace!("Built signal {} ({:?})", name, kind);

            Ok((
                i,
                Node::Signal(Signal {
                    name,
                    kind,
                    typ,
                    scalars,
                }),
            ))
        }
    }
}

fn hierarchy_section_inner<'input>(
    file: &GhwFile,
    i: Input<'input>,
) -> IResult<'input, HierarchySection> {
    let (i, (num_scopes, num_sigs, max_num_scalars)) = preceded(
        context("must be zero", tag("\0\0\0\0")),
        tuple((
            context("number of scopes", file.i32()),
            context("number of (composite) signals", file.i32()),
            context(
                "maximum number of scalar signals",
                map_res(file.i32(), usize::try_from),
            ),
        )),
    )(i)?;

    debug!(
        "Building {} signals (consisting of <={} scalars) in {} scopes",
        num_sigs, max_num_scalars, num_scopes
    );

    let mut scalars = Vec::with_capacity(max_num_scalars);
    let (i, (design, _)) = many_till(
        context("hierarchy node", |i| hierarchy_node(file, &mut scalars, i)),
        context(
            "end of hierarchy",
            verify(hierarchy::Kind::parse, |k| {
                *k == hierarchy::Kind::Special(SpecialKind::Eoh)
            }),
        ),
    )(i)?;

    debug!("Actual number of scalars: {}", scalars.len());

    Ok((i, HierarchySection { scalars, design }))
}

pub fn hierarchy_section(file: &GhwFile) -> impl Fn(Input) -> IResult<HierarchySection> + '_ {
    section(file, SectionKind::Hierarchy, hierarchy_section_inner)
}

#[derive(Clone, PartialEq, Debug)]
pub struct Snapshot {
    pub time: u64,
    pub values: Vec<ScalarValue>,
}

fn snapshot_inner<'input>(file: &GhwFile, i: Input<'input>) -> IResult<'input, Snapshot> {
    let (i, _) = context("must be zero", tag("\0\0\0\0"))(i)?;
    let (mut i, time) = context("simulation time", map_res(file.i64(), u64::try_from))(i)?;
    debug!("Reading snapshot for time {}", time);

    let mut values = Vec::new();
    for (n, scalar) in file.hierarchy.scalars.iter().enumerate() {
        let (tail, value) = context("signal value", |i| {
            scalar.parse_value(file.data_layout.endianness, i)
        })(i)?;
        i = tail;
        values.push(value);

        trace!("Scalar {} is {}", n, scalar.fmt_value(value));
    }

    let (i, _) = context("end of snapshot", tag("ESN\0"))(i)?;

    Ok((i, Snapshot { time, values }))
}

pub fn snapshot(file: &GhwFile) -> impl Fn(Input) -> IResult<Snapshot> + '_ {
    section(file, SectionKind::Snapshot, snapshot_inner)
}

#[derive(Clone, PartialEq, Debug)]
pub struct DeltaStep {
    pub time: u64,
    pub values: Vec<(PartIndex, ScalarValue)>,
}

fn step_values<'input>(
    file: &GhwFile,
    i: Input<'input>,
) -> IResult<'input, Vec<(PartIndex, ScalarValue)>> {
    let mut part_index: usize = 0;

    let (i, (values, _)) = many_till(
        flat_map(
            context(
                "part type",
                map_res(
                    context(
                        "delta from previous part index",
                        verify(map_res(leb128_u32, usize::try_from), |&x| x != 0),
                    ),
                    |index_delta| -> Result<_, String> {
                        part_index = part_index
                            .checked_add(index_delta)
                            .ok_or("part index out of range")?;

                        let real_index = part_index.checked_sub(1).ok_or(
                            "tried to modify part number 0, but part numbers start from 1",
                        )?;
                        let typ = file
                            .hierarchy
                            .scalars
                            .get(real_index)
                            .ok_or_else(|| format!("part index out of range: {real_index}"))?;

                        Ok((real_index, typ))
                    },
                ),
            ),
            |(index, typ)| {
                context("part value", move |i| {
                    let (i, value) = typ.parse_value(file.data_layout.endianness, i)?;
                    trace!("Scalar {} updated to {}", index, typ.fmt_value(value));

                    Ok((i, (index, value)))
                })
            },
        ),
        verify(leb128_u32, |&x| x == 0),
    )(i)?;
    Ok((i, values))
}

#[allow(clippy::shadow_unrelated)]
fn cycle_inner<'input>(file: &GhwFile, i: Input<'input>) -> IResult<'input, Vec<DeltaStep>> {
    let (i, mut time) = context("cycle start time", map_res(file.i64(), u64::try_from))(i)?;

    let mut deltas = Vec::new();

    let mut i_carry = i;
    loop {
        let i = i_carry;
        debug!("Reading delta cycle at time {}", time);
        let (i, step) = map(context("delta step", |i| step_values(file, i)), |values| {
            DeltaStep { time, values }
        })(i)?;
        deltas.push(step);

        let (i, time_delta) = context("time delta to next step", leb128_i64)(i)?;

        i_carry = i;

        if let Ok(time_delta) = u64::try_from(time_delta) {
            time += time_delta;
        } else if time_delta == -1 {
            break;
        } else {
            return error_custom(i, "negative time deltas are not allowed".to_owned());
        }
    }
    let i = i_carry;
    let (i, _) = tag("ECY\0")(i)?;
    Ok((i, deltas))
}

pub fn cycle(file: &GhwFile) -> impl Fn(Input) -> IResult<Vec<DeltaStep>> + '_ {
    section(file, SectionKind::Cycle, cycle_inner)
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Directory {
    sections: Vec<(SectionKind, usize)>,
}

fn directory_inner<'input>(file: &GhwFile, i: Input<'input>) -> IResult<'input, Directory> {
    debug!("Reading directory");
    map(
        delimited(
            context(
                "data layout",
                verify(DataLayout::parse, |&layout| layout == file.data_layout),
            ),
            length_count(
                context("number of sections", map_res(file.i32(), usize::try_from)),
                pair(
                    context("section type", SectionKind::parse_any),
                    context("section location", map_res(file.i32(), usize::try_from)),
                ),
            ),
            context("end of directory", tag("EOD\0")),
        ),
        |sections| Directory { sections },
    )(i)
}

pub fn directory(file: &GhwFile) -> impl Fn(Input) -> IResult<Directory> + '_ {
    section(file, SectionKind::Directory, directory_inner)
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Trailer {
    pub directory_location: usize,
}

fn trailer_inner<'input>(file: &GhwFile, i: Input<'input>) -> IResult<'input, Trailer> {
    debug!("Reading trailer");
    map(
        preceded(
            context(
                "data layout",
                verify(DataLayout::parse, |&layout| layout == file.data_layout),
            ),
            context("directory location", map_res(file.i32(), usize::try_from)),
        ),
        |directory_location| Trailer { directory_location },
    )(i)
}

pub fn trailer(file: &GhwFile) -> impl Fn(Input) -> IResult<Trailer> + '_ {
    section(file, SectionKind::Trailer, trailer_inner)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_build_prefix_string() {
        let mut prefix = b"abc".to_vec();
        let s = build_prefix_string(&mut prefix, b"def", 4);
        assert_eq!(s, Ok("abcdef".to_string()));
        assert_eq!(prefix, b"abcd".to_vec());
    }
}
