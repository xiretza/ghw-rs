use ghw::parse::{DataLayout, Endianness, GhwFile};

#[test]
fn counter() {
    let i = std::fs::read("tests/data/counter.ghw").unwrap();
    let (i, file) = GhwFile::parse(&i).unwrap();
    assert!(i.is_empty());

    assert_eq!(file.version, (0, 1));
    assert_eq!(
        file.data_layout,
        DataLayout {
            endianness: Endianness::Little,
            word_length: 4,
            offset_length: 1,
        }
    );

    let strings = file.strings;
    assert_eq!(strings.len(), 26);

    let types = file.types;
    assert_eq!(types.len(), 7);
}

#[test]
fn parity() {
    let i = std::fs::read("tests/data/parity.ghw").unwrap();
    let (i, file) = GhwFile::parse(&i).unwrap();
    assert!(i.is_empty());

    assert_eq!(file.version, (0, 1));
    assert_eq!(
        file.data_layout,
        DataLayout {
            endianness: Endianness::Little,
            word_length: 4,
            offset_length: 1,
        }
    );

    let strings = file.strings;
    assert_eq!(strings.len(), 19);

    let types = file.types;
    assert_eq!(types.len(), 2);
}
