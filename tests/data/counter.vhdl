library ieee;
use ieee.std_logic_1164.all,
	ieee.numeric_std.all;

entity counter_inner is
	generic (
		WIDTH : positive
	);
	port (
		clk : in std_logic;
		reset : in std_logic;
		run : in std_logic;
		up : in std_logic;
		count : out unsigned(WIDTH-1 downto 0)
	);
end entity;

architecture arch of counter_inner is
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if reset then
				count <= (others => '0');
			elsif run then
				if up then
					count <= count + 1;
				else
					count <= count - 1;
				end if;
			end if;
		end if;
	end process;
end architecture;

library ieee;
use ieee.std_logic_1164.all,
	ieee.numeric_std.all;

entity counter is
end entity;

architecture a of counter is
	signal clk, reset, run, up : std_logic;
	signal count : unsigned(15 downto 0);
begin
	uut: entity work.counter_inner
		generic map (
			WIDTH => 16
		)
		port map (
			clk => clk,
			reset => reset,
			run => run,
			up => up,
			count => count
		);

	process
		procedure pulse is
		begin
			clk <= '1';
			wait for 10 ns;
			clk <= '0';
			wait for 10 ns;
		end procedure;
	begin
		clk <= '0';
		reset <= '1';
		up <= '1';
		run <= '0';

		wait for 20 ns;

		pulse;
		assert count = x"0000";

		run <= '1';
		pulse;
		assert count = x"0000";

		reset <= '0';
		pulse;
		assert count = x"0001";

		up <= '0';
		pulse;
		assert count = x"0000";

		pulse;
		assert count = x"ffff";

		run <= '0';
		pulse;
		assert count = x"ffff";

		up <= '1';
		pulse;
		assert count = x"ffff";

		run <= '1';
		pulse;
		assert count = x"0000";

		pulse; pulse; pulse; pulse;
		assert count = x"0004";

		reset <= '1';
		pulse;
		assert count = x"0000";

		wait;
	end process;
end architecture;
