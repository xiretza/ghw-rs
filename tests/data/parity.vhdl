library ieee;
use ieee.std_logic_1164.all;

entity parity is
	generic (
		PARITY_EVEN : boolean
	);
	port (
		clk : in std_logic;
		reset : in std_logic;
		data : in std_logic;
		parity : out std_logic
	);
end entity;

architecture arch of parity is
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if reset then
				if PARITY_EVEN then
					parity <= '1';
				else
					parity <= '0';
				end if;
			else
				parity <= parity xor data;
			end if;
		end if;
	end process;
end architecture;
