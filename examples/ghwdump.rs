//! A reimplementation of the `ghwdump` utility using [`ghw_rs`].

use std::fmt::Write;
use std::fs;
use std::io;
use std::path::PathBuf;
use std::str::FromStr;

use nom::combinator::all_consuming;
use structopt::StructOpt;

use ghw::hierarchy::{BlockKind, Node, SignalKind};
use ghw::parse::GhwFile;
use ghw::types::{ParseValue, RootScalar, ScalarValue};

#[derive(Debug)]
struct SignalList {
    signals: Vec<usize>,
}

impl FromStr for SignalList {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, String> {
        let segments = s.split(',');
        let mut signals = Vec::new();

        for segment in segments {
            let nums = segment
                .split('-')
                .map(usize::from_str)
                .collect::<Result<Vec<_>, _>>()
                .map_err(|e| format!("Invalid signal index: {}", e))?;

            match nums.len() {
                1 => signals.push(nums[0]),
                2 => signals.extend(nums[0]..=nums[1]),
                _ => return Err(format!("Invalid signal filter syntax: {}", segment)),
            }
        }

        Ok(SignalList { signals })
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "ghwdump", about = "Display a GHDL Wavefile for debugging.")]
struct Opt {
    /// Display strings
    #[structopt(short = "S", long = "strings")]
    display_strings: bool,

    /// Display types
    #[structopt(short = "t", long = "types")]
    display_types: bool,

    /// Display hierarchy
    #[structopt(short = "h", long = "hierarchy")]
    display_hierarchy: bool,

    /// Display hierarchy with full path names
    #[structopt(short = "H", long = "hierarchy-full")]
    display_hierarchy_full: bool,

    /// Display time
    #[structopt(short = "T", long = "time")]
    display_time: bool,

    /// Display signals (and time)
    #[structopt(short = "s", long = "signals")]
    display_signals: bool,

    /// List of signals to display (example: -f 1,3,5-7,21-33)
    #[structopt(short = "f", long = "signal-filter")]
    signals: Option<SignalList>,

    /// Display list of sections
    #[structopt(short = "l", long = "sections")]
    display_sections: bool,

    /// Input files
    #[structopt(required(true), parse(from_os_str))]
    files: Vec<PathBuf>,
}

enum HierarchyMode {
    Indent(usize),
    Path(String),
}

fn print_hierarchy_node(node: Node, mode: &HierarchyMode) {
    if let HierarchyMode::Indent(indent) = mode {
        print!("{:1$}", "", indent);
    }

    let path_prefix: &str = match mode {
        HierarchyMode::Indent(_) => "",
        HierarchyMode::Path(p) => p,
    };

    match node {
        Node::Block(block) => {
            let mut extra_info = None;

            let kind = match block.kind {
                BlockKind::Design => "design",
                BlockKind::Block => "block",
                BlockKind::GenerateIf => "generate-if",
                BlockKind::GenerateFor { iterator_value, .. } => {
                    extra_info = Some(format!("({})", iterator_value));
                    "generate-for"
                }
                BlockKind::Instance => "instance",
                BlockKind::Package => "package",
                BlockKind::Process => "process",
                BlockKind::Generic => "generic",
            };

            println!(
                "{} {}{}: {}",
                kind,
                path_prefix,
                block.name,
                extra_info.unwrap_or_default()
            );

            let child_mode = match mode {
                HierarchyMode::Indent(x) => HierarchyMode::Indent(x + 1),
                HierarchyMode::Path(p) => HierarchyMode::Path(format!("{}{}/", p, block.name)),
            };

            for child in block.children {
                print_hierarchy_node(child, &child_mode);
            }
        }
        Node::Signal(signal) => {
            let kind = match signal.kind {
                SignalKind::Signal => "signal",
                SignalKind::PortIn => "port-in",
                SignalKind::PortOut => "port-out",
                SignalKind::PortInout => "port-inout",
                SignalKind::PortBuffer => "port-buffer",
                SignalKind::PortLinkage => "port-linkage",
            };

            print!(
                "{} {}{}: {}:",
                kind,
                path_prefix,
                signal.name,
                signal.typ.fmt_as_element()
            );

            let index_ranges = signal
                .scalars
                .iter()
                // displayed part indices are 1-indexed
                .map(|x| x + 1)
                // collapse consecutive indices into ranges
                .fold(Vec::new(), |mut ranges, index| {
                    match ranges.last_mut() {
                        Some((_, high)) if index == *high + 1 => *high = index,
                        _ => ranges.push((index, index)),
                    }

                    ranges
                });

            for (low, high) in index_ranges {
                print!(" #{}", low);
                if low != high {
                    print!("-#{}", high);
                }
            }

            println!();
        }
    }
}

fn run(opts: Opt) -> io::Result<()> {
    for filename in opts.files {
        let contents = fs::read(filename)?;

        let (_, file) = all_consuming(GhwFile::parse)(&contents).unwrap_or_else(|e| {
            eprintln!("Parsing GHW file failed!");
            eprintln!("{}", e);
            std::process::exit(1)
        });

        if opts.display_strings {
            println!("String table:");
            for string in file.strings {
                println!(" {}", string);
            }
        }

        if opts.display_types {
            for typ in file.types {
                if typ.name().is_some() {
                    print!("{};", typ);

                    let wkts = &file.well_known_types;
                    let wkt_idx = &[&wkts.boolean, &wkts.bit, &wkts.std_ulogic]
                        .iter()
                        .enumerate()
                        .find_map(|(i, wkt)| {
                            if wkt.as_ref()? == &typ {
                                Some(i + 1)
                            } else {
                                None
                            }
                        });
                    if let Some(wkt_idx) = wkt_idx {
                        println!("  -- WKT:{}", wkt_idx);
                    } else {
                        println!();
                    }
                }
            }
        }

        if opts.display_hierarchy || opts.display_hierarchy_full {
            let mode = if opts.display_hierarchy_full {
                HierarchyMode::Path("/".to_owned())
            } else {
                HierarchyMode::Indent(1)
            };

            println!("design");
            for node in file.hierarchy.design {
                print_hierarchy_node(node, &mode);
            }
        }

        if opts.display_time || opts.display_signals {
            fn display_values(scalars: &[RootScalar], values: &[ScalarValue]) {
                for (id, value) in values.iter().enumerate() {
                    let typ = &scalars[id];

                    let mut value_str = typ.fmt_value(*value);
                    if let RootScalar::Enum(..) = typ {
                        write!(&mut value_str, " ({})", value).unwrap();
                    }
                    println!("#{}: {}", id + 1, value_str)
                }
            }

            println!("Time is {} fs", file.snapshot.time);
            let mut values = file.snapshot.values;

            if opts.display_signals {
                display_values(&file.hierarchy.scalars, &values);
            }

            for delta in file.deltas {
                println!("Time is {} fs", delta.time);
                if opts.display_signals {
                    for (id, new_value) in delta.values {
                        values[id] = new_value
                    }

                    display_values(&file.hierarchy.scalars, &values);
                }
            }
        }
    }

    Ok(())
}

pub fn main() -> io::Result<()> {
    // Unignore SIGPIPE so print!() doesn't panic when the read end of the pipe is closed
    // https://github.com/rust-lang/rust/issues/46016
    unsafe {
        libc::signal(libc::SIGPIPE, libc::SIG_DFL);
    }

    env_logger::init();
    let opts = Opt::from_args();
    run(opts)
}
